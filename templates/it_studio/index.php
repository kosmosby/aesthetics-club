<?php
//  @copyright	Copyright (C) 2013 IceTheme. All Rights Reserved
//  @license	Copyrighted Commercial Software 
//  @author     IceTheme (icetheme.com)

defined('_JEXEC') or die;

// Include main PHP template file
include_once(JPATH_ROOT . "/templates/" . $this->template . '/php/template.php'); 

if ((JRequest::getCmd("tmpl", "index") != "offline")) { ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>

<?php if ($it_params_responsive == 1) { ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php } ?>

<jdoc:include type="head" />

	<?php
    // Include CSS and JS variables 
    include_once(IT_THEME_DIR.'/php/stylesheet.php');
    ?>
 
</head>

<body id="body" class="<?php echo htmlspecialchars($pageclass)?> <?php if ($it_mod_slideshow == 0) { ?>no_slideshow<?php } ?> <?php if ($it_params_fixednav == 1) { ?>param_fixed_header <?php } else { ?>param_static_header <?php } ?>"> 
<div id="main_wrapper">
            
        <header id="header" class="<?php if ($it_mod_slideshow == 0) { ?>no_slideshow<?php } ?> <?php if ($it_params_fixednav == 1) { ?>param_fixed_header <?php } else { ?>param_static_header <?php } ?>">
 			<div class="logo">
				<div class="container">
				<?php if ($it_params_logo != "") { ?>
                	<div id="logo" class="floatleft">	
                    	<a href="<?php echo $this->baseurl ?>"><?php echo $it_params_logo_img; ?></a>        	
                    </div>
                <?php } ?>
				<jdoc:include type="modules" name="callz" />
				</div>
			</div>       
            <nav id="mainmenu-container" class="clearfix" style="clear: both;">
            	
                <div class="container">
            
                    <div id="mainmenu-container-inside">
                    
                        <?php if ($it_mod_responsivebar != 0 && $it_params_responsive != 0) { ?>
                        <div id="mobile-btn" class="floatleft hidden-desktop">
                            <a id="responsive-menu-button" class="btn btn-navbar" href="#responsivebar-inner">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </a>
                        </div>
                        <script type="text/javascript">
                        jQuery(document).ready(function() {
                            jQuery(".btn-navbar").pageslide({
                                  direction: "right"
                            });
                        });
                        </script>
                        <?php } ?>   
                        

             
                        <nav id="mainmenu" class="floatright visible-desktop"> 
                                
                            <div class="navbar visible-desktop">
                                
                                <div class="navbar-inner">
                                    
                                    <div id="mainmenu_inside">
                                    
                                        <jdoc:include type="modules" name="mainmenu" />
                                        
                                        <?php if ($it_mod_search != 0 ) { ?> 
                                        <a href="#searchModal" class="search_icon" role="button" data-toggle="modal"><span><?php echo JText::_('TPL_SEARCH'); ?></span></a>
                                        <?php } ?>
                                        
                                        <?php if ($it_mod_language != 0) { ?>
                                        <div id="language" class="floatright visible-desktop">
                                            <jdoc:include type="modules" name="language" /> 
                                        </div>
                                        <?php } ?>
                                        
                                    </div>
                                    
                                </div>
                                 
                            </div>
                            
                        </nav>
                        
                    </div>
                    
                </div>
                
            </nav>

        </header>

		<?php if ($it_mod_slideshow != 0) { ?>
        <div id="slideshow" class="clearboth">
            <jdoc:include type="modules" name="slideshow" />
        </div>
        <?php } ?> 
        
		<?php if ($it_mod_breadcrumbs != 0) { ?>      
        <div id="breadcrumbs">
            <div class="container">
                <jdoc:include type="modules" name="breadcrumbs" />
            </div>
        </div>
        <?php } ?> 

        <section id="content">
       
            <div class="container">
                <jdoc:include type="message" />
            </div>    
  
            <?php if ($it_mod_promo != 0 ) { ?> 
            <div class="container">
                <div id="promo" class="animatedParent animateOnce" data-sequence="250" data-appear-top-offset="-150">
                    <div class="row-fluid">
                        <jdoc:include type="modules" name="promo" style="promo" />
                   </div> 
                   <hr class="sep">    
                </div>
            </div>
            <?php } ?> 
            
            <?php if ($it_mod_section1 != 0 ) { ?> 
            <div id="section1" class="ice_section <?php if ($it_params_fixednav == 1) { ?>param_fixed_header <?php } ?>">
                <jdoc:include type="modules" name="section1" style="" />
            </div>
            <?php } ?>
            
            <?php if ($it_mod_section2 != 0 ) { ?> 
            <div id="section2" class="ice_section <?php if ($it_params_fixednav == 1) { ?>param_fixed_header <?php } ?>">
                <jdoc:include type="modules" name="section2" style="" />
            </div>
            <?php } ?>
            
            <?php if ($it_mod_section3 != 0 ) { ?> 
            <div id="section3" class="ice_section <?php if ($it_params_fixednav == 1) { ?>param_fixed_header <?php } ?>">
                <jdoc:include type="modules" name="section3" style="" /> 
            </div>
            <?php } ?>
            
            <?php if ($it_mod_section4 != 0 ) { ?>
            <div id="section4" class="ice_section <?php if ($it_params_fixednav == 1) { ?>param_fixed_header <?php } ?>">
                <jdoc:include type="modules" name="section4" style="" />
            </div>
            <?php } ?>
            
            <?php if ($it_mod_section5 != 0 ) { ?> 
            <div id="section5" class="ice_section">
                <jdoc:include type="modules" name="section5" style="" />
            </div>
            <?php } ?>
             
            
            <?php if (!isset($it_hide_frontpage)) { ?>
            <div class="container">
           
                <div id="content_inner">
                
                    <div class="row-fluid">
                
                        <div id="middlecol" class="floatleft <?php echo $it_content_span;?> <?php echo $it_sidebar_pos_class; ?>">

                            <div class="inside"> 
                            
								<?php if ($it_mod_message != 0 ) { ?> 
                                <div id="message" class="alert alert-info">
                                	<jdoc:include type="modules" name="message" style="xhtml" />
                                </div>
                                <?php } ?>
                                
                                <jdoc:include type="component" />
                                
                            </div>
                            
                        </div>

                        <?php if ($it_mod_sidebar != 0) { ?> 
                        <div id="sidebar" class="<?php echo $it_sidebar_span;?> <?php echo $it_sidebar_pos_class; ?>" >
                            <div class="inside">  
                                <jdoc:include type="modules" name="sidebar" style="sidebar" />
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                       
                </div>  
                
            </div>
            <?php } ?>
                   
        </section><!-- / Content  -->
        
        <?php if (($it_mod_showcase || $it_mod_tagline) != 0) { ?>
        <section id="showcase" class="<?php if ($it_mod_tagline == 0 ) { ?>no-tagline<?php } ?> " data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">
                
            <div class="animatedParent animateOnce" data-appear-top-offset="-200" >
                    
                <div id="showcase_inner" class="container animated bounceInUp">
                    
                    <?php if ($it_mod_tagline != 0 ) { ?>
                    <div id="it_tagline" > 
                        <jdoc:include type="modules" name="tagline" style="xhtml" />
                    </div>    
                    <?php } ?> 
                
                    <?php if ($it_mod_showcase != 0 ) { ?>
                    <div id="showcase-modules"> 
                        <div class="row-fluid">
                            <jdoc:include type="modules" name="showcase" style="showcase" />
                        </div>
                    </div>    
                    <?php } ?> 
            
                </div>
                
            </div>
                         
        </section>
        <?php } ?>
        
        
        <footer id="footer">
        
            <div class="container">
               
                <?php if ($it_mod_footer != 0) { ?>
                
				<?php if (($it_mod_showcase || $it_mod_tagline) == 0) { ?>
                <hr class="sep footer_sep"/>
                <?php } ?> 
                
                <div class="row-fluid animatedParent animateOnce" data-sequence="250" data-appear-top-offset="-150">
                    <jdoc:include type="modules" name="footer" style="footer" />
                </div>
                <?php } ?>
            
            </div>
            
        </footer> 

       <?php if ($it_mod_responsivebar != 0 && $it_params_responsive != 0) { ?>
        <div id="responsivebar">
            <div id="responsivebar-inner">
                
                <?php if ($it_params_responsivebarlogo != "") { ?>
                <div id="responsivebarlogo">	
                   <a href="<?php echo $this->baseurl ?>"><?php echo $it_params_responsivebarlogo_img; ?></a>        	
                </div>
                <?php } ?>
                    
                <jdoc:include type="modules" name="responsivebar" style="xhtml" />
                
            </div>
        </div>    
        <?php } ?> 
        
        <?php if ($it_params_gotop != 0) { ?>
		<div id="gotop" class="">
			<a href="#" class="scrollup"><?php echo JText::_('TPL_SCROLL'); ?></a>
		</div>
		<?php } ?>
        
        <?php if ($it_mod_loginModal != 0 ) { ?> 
		<!-- Login Modal -->
		<div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel"><?php echo $modal_login_title; ?></h3>
			</div>
			
			<div class="modal-body">
				<jdoc:include type="modules" name="modal_login" />
			</div>
			
		</div>
		<?php } ?>  
        
        
        <?php if ($it_mod_search != 0 ) { ?> 
        <div id="searchModal" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
                        
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $modal_search_title; ?></h3>
          </div>
         
         <div class="modal-body">
            <jdoc:include type="modules" name="search" />
          </div>
          
        </div>
    	<?php } ?> 
        
        <?php 
            // if ($it_params_styleswitcher != 0) { 
                // Include style switcher JS 
                include_once(IT_THEME_DIR.'/php/style_switcher.php');
            // }
        ?>

        <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery.stellar({
                horizontalScrolling: false,
                responsive: true
            })
        });	
        </script>
        
        <?php if ($it_mod_responsivebar != 0 && $it_params_responsive != 0) { ?>
		<script src="<?php echo IT_THEME; ?>/assets/js/jquery.pageslide.min.js" type="text/javascript"></script> 
		<?php } ?>
        
        <?php if ($it_params_advanced_animations == 1) { ?>  
        <script src="<?php echo IT_THEME; ?>/assets/js/css3-animate-it.min.js" type="text/javascript"></script>
        <?php } ?>
        
    
    </div><!-- /MainWrapper -->
    
    <jdoc:include type="modules" name="debug" />
        
    <?php if ($it_params_fixednav == 1) { ?>        
	<script type="text/javascript">
   	/* MainMenu Fixed Navigation */
	jQuery(window).scroll(function(){
		if ( jQuery(this).scrollTop() > 1) {
			 jQuery("#header").addClass("header_fixed");
		} else {
			 jQuery("#header").removeClass("header_fixed");
		}
	}); 
    </script>
    <?php } ?>
    
    <!-- OnePage Plugin -->
    <script src="<?php echo IT_THEME; ?>/assets/js/jquery.nav.js" type="text/javascript"></script>
    
    <script type="text/javascript">
	jQuery(document).ready(function() {
		
		jQuery('#mainmenu_inside ul.nav, ul.nav_utility').onePageNav({
			currentClass: 'active',
			changeHash: false,
			scrollSpeed: 750,
			navItems: 'a',
			easing: 'swing',
			filter: '',
			scrollThreshold: 0.5,
			begin: false,
			end: false,
			scrollChange: false
		});
	});
    </script>
 
    <!-- Testimonials Owl Carousel -->
	<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(".about_testimonials").owlCarousel({
			nav : false,
			dots : true,
			dotsSpeed : 500,
			autoplay : false,
			autoplayTimeout: 5000,
			autoplaySpeed: 750,
			autoplayHoverPause: true,
			items:1,
			loop: false,
			mouseDrag: true,
			touchDrag: true,
			<?php if ($this->direction == 'rtl'){ echo ("rtl:true,"); }
				else { echo("rtl:false,");}
			?>
			
		})
	});
	</script>
    
    <!-- Statistics Owl Carousel -->
    <script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(".it_stats").owlCarousel({
			nav : false,
			dots : false,
			dotsSpeed : 500,
			autoplay : false,
			autoplayTimeout: 4500,
			autoplaySpeed: 750,
			autoplayHoverPause: true,
			items:1,
			loop: false,
			mouseDrag: true,
			touchDrag: true,
			<?php if ($this->direction == 'rtl'){ echo ("rtl:true,"); }
				else { echo("rtl:false,");}
			?>
			
		})
	});
	</script>
    
    <!-- Brands SimplyScroll -->
	<script type="text/javascript">
    (function(jQuery) {
        jQuery(function() { //on DOM ready 
            jQuery("#brands ul").simplyScroll();
        });
    })(jQuery);
    </script>
    
    <!-- Counter increase numbers with effect -->
	<script type="text/javascript">	
    jQuery(document).ready(function( jQuery ) {
        jQuery('.counter').counterUp({
            delay: 10,
            time: 1200
        });
    });
    </script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-45533167-6', 'auto');
    ga('send', 'pageview');

</script>
			
</body>
</html>
<?php } ?>