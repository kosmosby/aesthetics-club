<?php
// © IceTheme 2008 - 2014
// GNU General Public License

defined('_JEXEC') or die;

/**
 * This is a file to add template specific chrome to module rendering.  To use it you would
 * set the style attribute for the given module(s) include in your template to use the style
 * for each given modChrome function.
 *
 * eg.  To render a module mod_test in the submenu style, you would use the following include:
 * <jdoc:include type="module" name="test" style="submenu" />
 *
 * This gives template designers ultimate control over how modules are rendered.
 *
 * NOTICE: All chrome wrapping methods should be named: modChrome_{STYLE} and take the same
 * two arguments.
 */



// "Sidebar" module position
function modChrome_sidebar($module, &$params, &$attribs)
{
	 if(strpos($module->title,"|") !== false){
		$titleArray = explode("|",$module->title);		
		$module->title = "";
		for($i=0;$i<count($titleArray);$i++){
				$module->title .= $titleArray[$i];
		}		
	} 
	
	$headerLevel = $params->get('header_tag');
	
	if ($module->content) { ?>
		
		<div class="sidebar_module sidebar_module_<?php echo htmlspecialchars($params->get('moduleclass_sfx')) ?>">
        	
			     <?php if ($module->showtitle) {
                  
                  echo "<".$headerLevel." class=\"sidebar_module_heading\"><span>" . $module->title . "</span></".$headerLevel.">";
                    
               } ?>
			
                <div class="sidebar_module_content"><?php echo $module->content ?></div>
		
          </div>
          
         
	
    <?php  } } 
	

// Footer Modules
function modChrome_footer($module, &$params, &$attribs)
{ 

	static $modulecount;
	static $modules;
		
	global $it_mod_footer; //Number of Modules
	
	$animate_no=0; //Module Counter
	
	if ($it_mod_footer == 1) {
		$footer_width = "span12";
			
	} elseif ($it_mod_footer == 2) {
		$footer_width = "span6";
		
	} elseif ($it_mod_footer == 3) {
		$footer_width = "span4";
		
	} elseif ($it_mod_footer == 4) {
		$footer_width = "span3";
		
	} elseif ($it_mod_footer == 6) {	
		$footer_width = "span2";	
		
	} else {
		$footer_width = "span";	
	}
	
	
	if ($modulecount < 1)
	{
		$modulecount = count(JModuleHelper::getModules($module->position));
		$modules = array();
	}
	
	if ($modulecount == 1)
	{
		//Create a Class Temp to Store all Values
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->params = $module->params;
		$temp->id = $module->id;
		//Store all class elements in Array
		$modules[] = $temp;
		// list of moduletitles
		// list of moduletitles
	
		foreach ($modules as $rendermodule)
		{
			$animate_no++;
			
			if(strpos($rendermodule->title,"|") !== false){
				$titleArray = explode("|",$rendermodule->title);		
				$rendermodule->title = "";
				for($i=0;$i<count($titleArray);$i++){
					$rendermodule->title .= $titleArray[$i];
				}		
			}
			
			$headerLevel = $rendermodule->htag; //Store Header TAG
			$moduleSuffix = $rendermodule->csuffix; //Store Module Class Suffix

			echo '<div class="moduletable ' .$footer_width. ' animated fadeIn'.$moduleSuffix.'" data-id="'.$animate_no.'">';
				if ($rendermodule->showtitle) {
					echo '<'.$headerLevel.' class="moduletable_heading">' . $rendermodule->title . '</'.$headerLevel.'>';
				 } ?>
				<div class="moduletable_content clearfix">
					<?php echo $rendermodule->content; ?>
                </div>   
			</div>
            
		<?php }
	}
	else {
		//Duplicate Above content
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->params = $module->params;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->id = $module->id;
		$modules[] = $temp;
		$modulecount--;
	}
		   
}

// Promo Modules
function modChrome_promo($module, &$params, &$attribs)
{ 

	static $modulecount;
	static $modules;
		
	global $it_mod_promo; //Number of Modules
	
	$animate_no=0; //Module Counter
	
	if ($it_mod_promo == 1) {
		$promo_width = "span12";
			
	} elseif ($it_mod_promo == 2) {
		$promo_width = "span6";
		
	} elseif ($it_mod_promo == 3) {
		$promo_width = "span4";
		
	} elseif ($it_mod_promo == 4) {
		$promo_width = "span3";
		
	} elseif ($it_mod_promo == 6) {	
		$promo_width = "span2";	
		
	} else {
		$promo_width = "span";	
	}
	
	
	if ($modulecount < 1)
	{
		$modulecount = count(JModuleHelper::getModules($module->position));
		$modules = array();
	}
	
	if ($modulecount == 1)
	{
		//Create a Class Temp to Store all Values
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->params = $module->params;
		$temp->id = $module->id;
		//Store all class elements in Array
		$modules[] = $temp;
		// list of moduletitles
		// list of moduletitles
	
		foreach ($modules as $rendermodule)
		{
			$animate_no++;
			
			if(strpos($rendermodule->title,"|") !== false){
				$titleArray = explode("|",$rendermodule->title);		
				$rendermodule->title = "";
				for($i=0;$i<count($titleArray);$i++){
					$rendermodule->title .= $titleArray[$i];
				}		
			}
			
			$headerLevel = $rendermodule->htag; //Store Header TAG
			$moduleSuffix = $rendermodule->csuffix; //Store Module Class Suffix

			echo '<div class="moduletable ' .$promo_width. ' animated fadeIn'.$moduleSuffix.'" data-id="'.$animate_no.'">';
				if ($rendermodule->showtitle) {
					echo '<'.$headerLevel.' class="moduletable_heading">' . $rendermodule->title . '</'.$headerLevel.'>';
				 } ?>
				<div class="moduletable_content clearfix">
					<?php echo $rendermodule->content; ?>
                </div>   
			</div>
            
		<?php }
	}
	else {
		//Duplicate Above content
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->params = $module->params;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->id = $module->id;
		$modules[] = $temp;
		$modulecount--;
	}
		   
}

/*
"Social Area (tabs)" position */

function modChrome_social_tabs($module, &$params, &$attribs)
{ 
	
	static $modulecount;
	static $modules;
	
	$datapane_li=0;
	$datapane_id=0;
	
	if ($modulecount < 1)
	{
		$modulecount = count(JModuleHelper::getModules($module->position));
		$modules = array();
	}

	if ($modulecount == 1)
	{
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->title = $module->title;
		$temp->params = $module->params;
		$temp->id = $module->id;
		$modules[] = $temp;
		// list of moduletitles
		// list of moduletitles
		

		echo '<div class=""><ul class="nav nav-tabs responsive" id="social_tabs_mod">';

		foreach ($modules as $rendermodule)
		{
			$datapane_li++;
			
			$activeclass = "";
            if($datapane_li == 1){
                $activeclass = "active";
            }
			
			echo '<li class="'.$activeclass.'"><a href="#pane'. $datapane_li .'" data-toggle="tab">'.$rendermodule->title.'</a></li>';
		}
		echo '</ul>';
		
		echo '<div class="tab-content responsive">';
		// modulecontent
		foreach ($modules as $rendermodule)
		{
			$datapane_id++;
			
			$activeclass = "";
            if($datapane_id == 1){
                $activeclass = "active";
            }
			
			echo '<div id="pane'. $datapane_id .'" class="tab-pane '.$activeclass.'">'. $rendermodule->content.'</div>';
		}
		
		echo '</div>';
		
		echo '</div>';
		
		echo '<script>
		 	  
		  (function($) {
			  fakewaffle.responsiveTabs([\'phone\', \'tablet\']);
		  })(jQuery);


		</script>';
		
	} else {
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->params = $module->params;
		$temp->title = $module->title;
		$temp->id = $module->id;
		$modules[] = $temp;
		$modulecount--;
	}
           
 } 


// Showcase Modules
 
function modChrome_showcase($module, &$params, &$attribs)
{
	global $it_mod_showcase;
	
	if ($it_mod_showcase == 1) {
		$showcase_width = "span12";
			
	} elseif ($it_mod_showcase == 2) {
		$showcase_width = "span6";
		
	} elseif ($it_mod_showcase == 3) {
		$showcase_width = "span4";
		
	} elseif ($it_mod_showcase == 4) {
		$showcase_width = "span3";
		
	} elseif ($it_mod_showcase == 6) {	
		$showcase_width = "span2";	
		
	} else {
		$showcase_width = "span";	
	}
	
	if(strpos($module->title,"|") !== false){
		$titleArray = explode("|",$module->title);		
		$module->title = "";
		for($i=0;$i<count($titleArray);$i++){
			$module->title .= $titleArray[$i];
		}		
	}
	
	$headerLevel = $params->get('header_tag');
	
	if (!empty ($module->content)) : ?>
         
       <div class="moduletable <?php echo $showcase_width; ?>">
        
			<?php if ($module->showtitle) {
				 echo '<'.$headerLevel.' class="moduletable_heading">' . $module->title . '</'.$headerLevel.'>';
			} ?>
        	
             <div class="moduletable_content clearfix">
			 <?php echo $module->content; ?>
             </div>
                
		</div>
	<?php endif;
}





// Banner Modules
function modChrome_banner($module, &$params, &$attribs)
{
	global $it_mod_banner;
	
	if ($it_mod_banner == 1) {
		$banner_width = "span12";
			
	} elseif ($it_mod_banner == 2) {
		$banner_width = "span6";
		
	} elseif ($it_mod_banner == 3) {
		$banner_width = "span4";
		
	} elseif ($it_mod_banner == 4) {
		$banner_width = "span3";
		
	} elseif ($it_mod_banner == 6) {	
		$banner_width = "span2";	
		
	} else {
		$banner_width = "span";	
	}
	
	$headerLevel = $params->get('header_tag');
	
	if (!empty ($module->content)) : ?>
         
       <div class="moduletable <?php echo $banner_width; ?>">
        
			<?php if ($module->showtitle) {
				 echo '<'.$headerLevel.' class="moduletable_heading">' . $module->title . '</'.$headerLevel.'>';
			} ?>
        	
             <div class="moduletable_content clearfix">
			 <?php echo $module->content; ?>
             </div>
                
		</div>
	<?php endif;
}

// Testimonial Modules
function modChrome_testimonials($module, &$params, &$attribs)
{ 

	static $modulecount;
	static $modules;
		
	global $it_mod_testimonials; //Number of Modules
	
	if ($modulecount < 1)
	{
		$modulecount = count(JModuleHelper::getModules($module->position));
		$modules = array();
	}
	
	if ($modulecount == 1)
	{
		//Create a Class Temp to Store all Values
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->params = $module->params;
		$temp->id = $module->id;
		//Store all class elements in Array
		$modules[] = $temp;
		// list of moduletitles
		// list of moduletitles
	
		echo '<div id="testimonial_wrapper">';
			echo '<div class="about_testimonials">';
		
				foreach ($modules as $rendermodule)
				{
					
					if(strpos($rendermodule->title,"|") !== false){
						$titleArray = explode("|",$rendermodule->title);		
						$rendermodule->title = "";
						for($i=0;$i<count($titleArray);$i++){
							$rendermodule->title .= $titleArray[$i];
						}		
					}
					
					$headerLevel = $rendermodule->htag; //Store Header TAG
					$moduleSuffix = $rendermodule->csuffix; //Store Module Class Suffix
		
					echo '<div class=" ' .$moduleSuffix.'">';
						if ($rendermodule->showtitle) {
							echo '<'.$headerLevel.' class="moduletable_heading">' . $rendermodule->title . '</'.$headerLevel.'>';
						 } ?>
						<div class="moduletable_content clearfix">
							<?php echo $rendermodule->content; ?>
						</div>   
					</div>
					
				<?php }
		
			echo '</div>';
		echo '</div>';
	}
	else {
		//Duplicate Above content
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->params = $module->params;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->id = $module->id;
		$modules[] = $temp;
		$modulecount--;
	}
		   
}

// Statistics Modules
function modChrome_statistics($module, &$params, &$attribs)
{ 

	static $modulecount;
	static $modules;
		
	global $it_mod_statistics; //Number of Modules
	
	if ($modulecount < 1)
	{
		$modulecount = count(JModuleHelper::getModules($module->position));
		$modules = array();
	}
	
	if ($modulecount == 1)
	{
		//Create a Class Temp to Store all Values
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->params = $module->params;
		$temp->id = $module->id;
		//Store all class elements in Array
		$modules[] = $temp;
		// list of moduletitles
		// list of moduletitles
	
		// echo '<div id="testimonial_wrapper">';
		echo '<div class="ice_portfolio_stats" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">';
			echo '<div class="">';
				echo '<div class="it_stats">';
			
					foreach ($modules as $rendermodule)
					{
						
						if(strpos($rendermodule->title,"|") !== false){
							$titleArray = explode("|",$rendermodule->title);		
							$rendermodule->title = "";
							for($i=0;$i<count($titleArray);$i++){
								$rendermodule->title .= $titleArray[$i];
							}		
						}
						
						$headerLevel = $rendermodule->htag; //Store Header TAG
						$moduleSuffix = $rendermodule->csuffix; //Store Module Class Suffix
			
						echo '<div class=" ' .$moduleSuffix.'">';
							if ($rendermodule->showtitle) {
								echo '<'.$headerLevel.' class="moduletable_heading">' . $rendermodule->title . '</'.$headerLevel.'>';
							 } ?>
							<div class="moduletable_content clearfix">
								<?php echo $rendermodule->content; ?>
							</div>   
						</div>
						
					<?php }
			
				echo '</div>';
			echo '</div>';
		echo '</div>';
		// echo '</div>';
	}
	else {
		//Duplicate Above content
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->params = $module->params;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->id = $module->id;
		$modules[] = $temp;
		$modulecount--;
	}
		   
}

// Services Modules Animated
function modChrome_animated($module, &$params, &$attribs)
{ 

	static $modulecount;
	static $modules;
		
	global $it_mod_services_animated; //Number of Modules
	
	$animate_no=0; //Module Counter for Animations
	
	if ($modulecount < 1)
	{
		$modulecount = count(JModuleHelper::getModules($module->position));
		$modules = array();
	}

	if ($modulecount == 1)
	{
		//Create a Class Temp to Store all Values
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->params = $module->params;
		$temp->id = $module->id;
		//Store all class elements in Array
		$modules[] = $temp;
		// list of moduletitles
		// list of moduletitles
		
		// Variable holder for desired width (Syntax expected "ice_col_1/2/3/4/6/12"
		$ice_width = $temp->csuffix;
		// Variable Holder for Span-width
		$ice_span_width = 12;
		// Variable Holder for Divider Location
		$ice_divider = 1;
		
		if (strpos($ice_width,'ice_col_1') !== false) {
			$ice_span_width = 12;
			$ice_divider = 1;
		}
		elseif (strpos($ice_width,'ice_col_2') !== false) {
			$ice_span_width = 6;
			$ice_divider = 2;
		}
		elseif (strpos($ice_width,'ice_col_3') !== false) {
			$ice_span_width = 4;
			$ice_divider = 3;
		}
		elseif (strpos($ice_width,'ice_col_4') !== false) {
			$ice_span_width = 3;
			$ice_divider = 4;
		}
		elseif (strpos($ice_width,'ice_col_6') !== false) {
			$ice_span_width = 2;
			$ice_divider = 6;
		}
		elseif (strpos($ice_width,'ice_col_12') !== false) {
			$ice_span_width = 12;
			$ice_divider = 1;
		}
		// All other cases that Bootstrap does not recognise will be displayed at full-width
		elseif (strpos($ice_width,'ice_col_') !== false) {
			$ice_span_width = 12;
			$ice_divider = 1;
		}

		// Opening TAG For Animated Parent
		echo '<div class="animatedParent animateOnce" data-appear-top-offset="-250" data-sequence="250">';
	
			// Layout will be generated. Set Counter to 0 for openers and closers of row-fluids
			$counter_start = -1;
			$counter_finish = 0;
			
			// Variables to count total Openers and closers of row-fluid divs
			$ice_total_openers = 0;
			$ice_total_closers = 0;
			
			foreach ($modules as $rendermodule)
			{
				// Breaker Calculator for Opening row-fluid divs
				$breaker = (( ++$counter_start % $ice_divider == 0 ) ? true : false );
				// Open row-fluid for each group of modules
				if ( $breaker ) {
                	echo '<div class="row-fluid">';
					$ice_total_openers = $ice_total_openers + 1;
				}
				
				$animate_no++;
				
				if(strpos($rendermodule->title,"|") !== false){
					$titleArray = explode("|",$rendermodule->title);		
					$rendermodule->title = "";
					for($i=0;$i<count($titleArray);$i++){
						$rendermodule->title .= $titleArray[$i];
					}
				}
				
				$headerLevel = $rendermodule->htag; //Store Header TAG
				$moduleSuffix = $rendermodule->csuffix; //Store Module Class Suffix
	
				echo '<div class="moduletable animated fadeIn '.$moduleSuffix.' span'.$ice_span_width.'" data-id="'.$animate_no.'">';
					if ($rendermodule->showtitle) {
						echo '<'.$headerLevel.' class="moduletable_heading">' . $rendermodule->title . '</'.$headerLevel.'>';
					 } ?>
					<div class="moduletable_content clearfix">
						<?php echo $rendermodule->content; ?>
					</div>   
				</div>

                <?php // Breaker Calculator for Closing row-fluid divs
				$breaker_finish = (( ++$counter_finish % $ice_divider == 0 ) ? true : false );
				// Close row-fluid for each group of modules
				if ( $breaker_finish ) {
                	echo '</div>';
					$ice_total_closers = $ice_total_closers + 1;
				}
				?>
                
                
                
				
			<?php }
			
			// final row-fluid closer if there are no more modules to finish the condition above
			$ice_total_closers = $ice_total_closers + 1;
			if ( $ice_total_openers == $ice_total_closers ) {
					echo '</div>';
				}
		
		// Closing TAG for animated Parent
		echo '</div>';
	}
	
	else {
		//Duplicate Above content
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->params = $module->params;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->id = $module->id;
		$modules[] = $temp;
		$modulecount--;
	}

}


// Our Members Modules Animated
function modChrome_animembers($module, &$params, &$attribs)
{ 

	static $modulecount;
	static $modules;
		
	global $it_mod_members_animated; //Number of Modules
	
	$animate_no=0; //Module Counter for Animations
	
	if ($modulecount < 1)
	{
		$modulecount = count(JModuleHelper::getModules($module->position));
		$modules = array();
	}

	if ($modulecount == 1)
	{
		//Create a Class Temp to Store all Values
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->params = $module->params;
		$temp->id = $module->id;
		//Store all class elements in Array
		$modules[] = $temp;
		// list of moduletitles
		// list of moduletitles
		
		// Variable holder for desired width (Syntax expected "ice_col_1/2/3/4/6/12"
		$ice_width = $temp->csuffix;
		// Variable Holder for Span-width
		$ice_span_width = 12;
		// Variable Holder for Divider Location
		$ice_divider = 1;
		
		if (strpos($ice_width,'ice_col_1') !== false) {
			$ice_span_width = 12;
			$ice_divider = 1;
		}
		elseif (strpos($ice_width,'ice_col_2') !== false) {
			$ice_span_width = 6;
			$ice_divider = 2;
		}
		elseif (strpos($ice_width,'ice_col_3') !== false) {
			$ice_span_width = 4;
			$ice_divider = 3;
		}
		elseif (strpos($ice_width,'ice_col_4') !== false) {
			$ice_span_width = 3;
			$ice_divider = 4;
		}
		elseif (strpos($ice_width,'ice_col_6') !== false) {
			$ice_span_width = 2;
			$ice_divider = 6;
		}
		elseif (strpos($ice_width,'ice_col_12') !== false) {
			$ice_span_width = 12;
			$ice_divider = 1;
		}
		// All other cases that Bootstrap does not recognise will be displayed at full-width
		elseif (strpos($ice_width,'ice_col_') !== false) {
			$ice_span_width = 12;
			$ice_divider = 1;
		}

		// Opening TAG For Animated Parent
		echo '<div class="animatedParent animateOnce" data-appear-top-offset="-250" data-sequence="250">';
	
			// Layout will be generated. Set Counter to 0 for openers and closers of row-fluids
			$counter_start = -1;
			$counter_finish = 0;
			
			// Variables to count total Openers and closers of row-fluid divs
			$ice_total_openers = 0;
			$ice_total_closers = 0;
			
			foreach ($modules as $rendermodule)
			{
				// Breaker Calculator for Opening row-fluid divs
				$breaker = (( ++$counter_start % $ice_divider == 0 ) ? true : false );
				// Open row-fluid for each group of modules
				if ( $breaker ) {
                	echo '<div class="row-fluid">';
					$ice_total_openers = $ice_total_openers + 1;
				}
				
				$animate_no++;
				
				if(strpos($rendermodule->title,"|") !== false){
					$titleArray = explode("|",$rendermodule->title);		
					$rendermodule->title = "";
					for($i=0;$i<count($titleArray);$i++){
						$rendermodule->title .= $titleArray[$i];
					}
				}
				
				$headerLevel = $rendermodule->htag; //Store Header TAG
				$moduleSuffix = $rendermodule->csuffix; //Store Module Class Suffix
	
				echo '<div class="moduletable animated fadeIn '.$moduleSuffix.' span'.$ice_span_width.'" data-id="'.$animate_no.'">';
					if ($rendermodule->showtitle) {
						echo '<'.$headerLevel.' class="moduletable_heading">' . $rendermodule->title . '</'.$headerLevel.'>';
					 } ?>
					<div class="moduletable_content clearfix">
						<?php echo $rendermodule->content; ?>
					</div>   
				</div>

                <?php // Breaker Calculator for Closing row-fluid divs
				$breaker_finish = (( ++$counter_finish % $ice_divider == 0 ) ? true : false );
				// Close row-fluid for each group of modules
				if ( $breaker_finish ) {
                	echo '</div>';
					$ice_total_closers = $ice_total_closers + 1;
				}
				?>
                
                
                
				
			<?php }
			
			// final row-fluid closer if there are no more modules to finish the condition above
			$ice_total_closers = $ice_total_closers + 1;
			if ( $ice_total_openers == $ice_total_closers ) {
					echo '</div>';
				}
		
		// Closing TAG for animated Parent
		echo '</div>';
	}
	
	else {
		//Duplicate Above content
		$temp = new stdClass;
		$temp->content = $module->content;
		$temp->params = $module->params;
		$temp->title = $module->title;
		$temp->htag = $params->get('header_tag');
		$temp->csuffix = $params->get('moduleclass_sfx');
		$temp->showtitle = $module->showtitle;
		$temp->id = $module->id;
		$modules[] = $temp;
		$modulecount--;
	}

}

?>