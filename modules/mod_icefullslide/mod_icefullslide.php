<?php

#@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

defined('_JEXEC') or die;
require_once(dirname(__FILE__).'/helper.php');

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$listofimages = mod_icefullslideHelper::getImages($params);
mod_icefullslideHelper::load_jquery($params);

$doc = JFactory::getDocument();

$doc->addStyleSheet(JURI::base(true) . '/modules/mod_icefullslide/assets/css/superslides.css', 'text/css' );
$doc->addScript(JURI::base(true) . '/modules/mod_icefullslide/assets/js/jquery.superslides.min.js');

require(JModuleHelper::getLayoutPath('mod_icefullslide'));
