<?php

#@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

defined('_JEXEC') or die;

?>

<div id="slides-<?php echo $module->id; ?>" class="icefullslide icefullslide_preload">

    <ul class="slides-container">
    <?php
        foreach($listofimages as $item){
            echo $item;
        }
    ?>
    </ul>
    
    <nav class="slides-navigation">
        <a class="next" href="#"><span>Next &rsaquo;</span></a>
        <a class="prev" href="#"><span>&lsaquo; Previous</span></a>
    </nav>
    
    <div class="icefullslide_preload_icon"></div>
    
</div>

<script type="text/javascript">
	
	<!-- Superslides Plugin for Full-screen IceSlideshow -->
	jQuery('#slides-<?php echo $module->id; ?>').superslides({
		animation: '<?php echo $params->get('fadeorslide');?>',
		pagination: <?php echo $params->get('pagination');?>,
		play: <?php echo $params->get('autoplayspeed');?>,
		animation_speed: '<?php echo $params->get('animationspeed');?>',
		animation_easing: 'linear'
	});
	
	jQuery(window).load (function () { 
	  jQuery('#slides-<?php echo $module->id; ?>').removeClass('icefullslide_preload')
	});

</script>
